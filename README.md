# Material for a stand about "Linux on Smartphones"

License: [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/). The license only applies to the contents in this repo, not to the submodules' contents.